package id.ac.cs.ui.tutorial5.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import id.ac.cs.ui.tutorial5.service.SoulServiceImpl;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = SoulController.class)
public class SoulControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SoulServiceImpl soulService;

    @Test
    public void whenSoulHomeURIIsAccessedItShouldHandledByFindAll() throws Exception {
        mockMvc.perform(get("/soul/")).andExpect(status().isOk()).andExpect(handler().methodName("findAll"));
    }

}