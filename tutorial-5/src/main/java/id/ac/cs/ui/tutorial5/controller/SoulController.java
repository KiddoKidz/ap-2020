package id.ac.cs.ui.tutorial5.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import id.ac.cs.ui.tutorial5.core.*;
import id.ac.cs.ui.tutorial5.service.*;

// TODO: Import apapun yang anda perlukan agar controller ini berjalan
@RestController
@RequestMapping(path = "/soul")
public class SoulController {

    @Autowired
    private SoulService soulService;

    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        return ResponseEntity.ok().body(soulService.findAll());
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity create(@RequestBody Soul soul) {
        soulService.register(soul);
        return ResponseEntity.ok("Success Create");
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id) {
        return ResponseEntity.ok().body(soulService.findSoul(id).get());
    }

    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        if (soul.getId() == id) {
            return ResponseEntity.ok().body(soulService.rewrite(soul));
        }
        return ResponseEntity.ok(soul);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        soulService.erase(id);
        return ResponseEntity.ok("Success Delete");
    }
}