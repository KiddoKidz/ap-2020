package id.ac.cs.ui.tutorial5.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import id.ac.cs.ui.tutorial5.core.Soul;

// TODO: Import Soul.java

public interface SoulRepository extends JpaRepository<Soul, Long> {
}
