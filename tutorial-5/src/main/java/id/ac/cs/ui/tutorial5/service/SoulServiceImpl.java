
package id.ac.cs.ui.tutorial5.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.cs.ui.tutorial5.core.Soul;
import id.ac.cs.ui.tutorial5.repository.SoulRepository;

@Service
public class SoulServiceImpl implements SoulService {

    @Autowired
    private SoulRepository repository;

    @Override
    public List<Soul> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Soul> findSoul(Long id) {
        return repository.findById(id);
    }

    @Override
    public void erase(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Soul rewrite(Soul soul) {
        return repository.save(soul);
    }

    @Override
    public Soul register(Soul soul) {
        return repository.save(soul);
    }

}