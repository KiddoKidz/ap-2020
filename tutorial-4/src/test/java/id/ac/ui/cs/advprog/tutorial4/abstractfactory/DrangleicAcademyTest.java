package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        this.drangleicAcademy = new DrangleicAcademy();
        this.majesticKnight = this.drangleicAcademy.getKnight("majestic");
        this.metalClusterKnight = this.drangleicAcademy.getKnight("metal cluster");
        this.syntheticKnight = this.drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(this.majesticKnight instanceof MajesticKnight);
        assertTrue(this.metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(this.syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals(this.majesticKnight.getName(), "Majestic Knight");
        assertEquals(this.metalClusterKnight.getName(), "Metal Cluster Knight");
        assertEquals(this.syntheticKnight.getName(), "Synthetic Knight");
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertTrue(this.majesticKnight.getArmor() instanceof MetalArmor);
        assertTrue(this.majesticKnight.getWeapon() instanceof ThousandJacker);

        assertTrue(this.metalClusterKnight.getArmor() instanceof MetalArmor);
        assertTrue(this.metalClusterKnight.getSkill() instanceof ThousandYearsOfPain);

        assertTrue(this.syntheticKnight.getWeapon() instanceof ThousandJacker);
        assertTrue(this.syntheticKnight.getSkill() instanceof ThousandYearsOfPain);
    }

}
