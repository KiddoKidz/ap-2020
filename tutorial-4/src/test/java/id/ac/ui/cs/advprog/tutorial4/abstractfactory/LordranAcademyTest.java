package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        this.drangleicAcademy = new LordranAcademy();
        this.majesticKnight = this.drangleicAcademy.getKnight("majestic");
        this.metalClusterKnight = this.drangleicAcademy.getKnight("metal cluster");
        this.syntheticKnight = this.drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(this.majesticKnight instanceof MajesticKnight);
        assertTrue(this.metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(this.syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals(this.majesticKnight.getName(), "Majestic Knight");
        assertEquals(this.metalClusterKnight.getName(), "Metal Cluster Knight");
        assertEquals(this.syntheticKnight.getName(), "Synthetic Knight");
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertTrue(this.majesticKnight.getArmor() instanceof ShiningArmor);
        assertTrue(this.majesticKnight.getWeapon() instanceof ShiningBuster);

        assertTrue(this.metalClusterKnight.getArmor() instanceof ShiningArmor);
        assertTrue(this.metalClusterKnight.getSkill() instanceof ShiningForce);

        assertTrue(this.syntheticKnight.getWeapon() instanceof ShiningBuster);
        assertTrue(this.syntheticKnight.getSkill() instanceof ShiningForce);
    }
}
