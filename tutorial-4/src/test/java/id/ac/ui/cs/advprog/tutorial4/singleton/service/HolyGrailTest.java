package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.Mock;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    @Mock
    private HolyGrail holyGrail;
    // TODO create tests
    @Test
    public void testGetAWish() {
        Mockito.when(holyGrail.getHolyWish()).thenReturn(HolyWish.getInstance());
        assertTrue(holyGrail.getHolyWish() instanceof HolyWish);
    }

}
