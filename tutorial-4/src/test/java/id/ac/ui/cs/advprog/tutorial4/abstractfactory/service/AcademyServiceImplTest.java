package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    // TODO create tests
    @Test
    public void makeKnightAcademyByName(){
        Mockito.when(academyRepository.getKnightAcademyByName("Lordran")).thenReturn(new LordranAcademy());
        academyService.produceKnight("Lordran", "synthetic");
        verify(academyRepository, times(1)).getKnightAcademyByName("Lordran");
    }

    @Test
    public void getKnightAcademy(){
        academyService.getKnightAcademies();
        verify(academyRepository, times(1)).getKnightAcademies();
    }
}
