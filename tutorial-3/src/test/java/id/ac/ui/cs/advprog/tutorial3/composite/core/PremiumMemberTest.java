package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
        guildMaster = new PremiumMember("Lord", "Master");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals(member.getName(), "Wati");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals(member.getRole(), "Gold Merchant");
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        member.addChildMember(new OrdinaryMember("Loli", "Silver Merchant"));
        assertEquals(member.getChildMembers().size(), 1);
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member memTest = new OrdinaryMember("Loli", "Silver Merchant");
        member.addChildMember(memTest);
        assertEquals(member.getChildMembers().size(), 1);
        member.removeChildMember(memTest);
        assertEquals(member.getChildMembers().size(), 0);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member memTest0 = new OrdinaryMember("Loli", "Silver Merchant");
        Member memTest1 = new OrdinaryMember("Lolipop", "Gold Merchant");
        Member memTest2 = new OrdinaryMember("Lolupip", "Bronze Merchant");
        Member memTest3 = new OrdinaryMember("Loliv", "Copper Merchant");
        member.addChildMember(memTest0);
        member.addChildMember(memTest1);
        member.addChildMember(memTest2);
        member.addChildMember(memTest3);
        assertEquals(member.getChildMembers().size(), 3);

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member memTest0 = new OrdinaryMember("Loli", "Silver Merchant");
        Member memTest1 = new OrdinaryMember("Lolipop", "Gold Merchant");
        Member memTest2 = new OrdinaryMember("Lolupip", "Bronze Merchant");
        Member memTest3 = new OrdinaryMember("Loliv", "Copper Merchant");
        guildMaster.addChildMember(memTest0);
        guildMaster.addChildMember(memTest1);
        guildMaster.addChildMember(memTest2);
        guildMaster.addChildMember(memTest3);
        assertEquals(guildMaster.getChildMembers().size(), 4);
    }
}
