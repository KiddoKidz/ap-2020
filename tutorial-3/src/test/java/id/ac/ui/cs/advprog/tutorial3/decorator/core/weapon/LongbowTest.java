package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongbowTest {


    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Longbow();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals(weapon.getName(), "Longbow");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        assertEquals(weapon.getDescription(), "Big Longbow");
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        assertEquals(weapon.getWeaponValue(), 15);
    }
}
