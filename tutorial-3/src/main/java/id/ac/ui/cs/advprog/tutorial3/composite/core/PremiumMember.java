package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        //TODO: Complete me
        private String name;
        private String role;
        private ArrayList<Member> children;

        public PremiumMember(String name, String role){
                this.name = name;
                this.role = role;
                this.children = new ArrayList<Member>();
        };

        public String getName() {
                return this.name;
        }

        public String getRole() {
                return this.role;
        }

        public void addChildMember(Member member) {
                if(this.children.size()<3){
                        this.children.add(member);                
                } else if (this.role.equals("Master")){
                        this.children.add(member);
                }
        }

        public void removeChildMember(Member member){
                this.children.remove(member);
        }

        public List<Member> getChildMembers(){
                return this.children;
        }
}
