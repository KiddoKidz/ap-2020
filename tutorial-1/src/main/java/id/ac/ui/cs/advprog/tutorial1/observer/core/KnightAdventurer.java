package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

        public KnightAdventurer(Guild guild) {
                this.name = "Knight";
                //ToDo: Complete Me
                this.guild = guild;
                guild.add(this);
        }

        //ToDo: Complete Me
        public void update(){
                if(guild.getQuest().getType().equals("D") || guild.getQuest().getType().equals("E") || guild.getQuest().getType().equals("R")){
                        this.getQuests().add(this.guild.getQuest());
                }
        }
}
